/* Renato Cola�o e Bruno Maia */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Globo {
	
	// constantes
	private final int NUMERO_MINIMO;
	private final int NUMERO_MAXIMO;
	
	// vari�veis de inst�ncia
	//private List<Integer> numerosSorteados;
	private List<Integer> numerosNoGlobo;
	
	// construtor
	public Globo(int numeroMinimo, int numeroMaximo){
		NUMERO_MINIMO = numeroMinimo;
		NUMERO_MAXIMO = numeroMaximo;
		
		//n�meros no globo
		Integer[] numeros = BingoHelper.getNumerosGlobo(NUMERO_MINIMO, NUMERO_MAXIMO);
		numerosNoGlobo = new ArrayList<Integer>( Arrays.asList(numeros));
	}
	
	// m�todos privados
			
	
	// m�todos p�blicos
	public int sortearNumero(){
		
		// consiste em retornar o n�mero sorteado
		// e remover o n�mero de dentro do globo
		
		Integer numero = null;
		
		while (numero == null) {
			
			// nextInt is normally exclusive of the top value,
			// so add 1 to make it inclusive
			int random = ThreadLocalRandom
					.current()
					.nextInt(NUMERO_MINIMO, NUMERO_MAXIMO + 1);
			
			int index = numerosNoGlobo.indexOf(random);
			if(index != -1){
				
				numero = random;
				numerosNoGlobo.remove(index);
				
			}			
		}		
		return numero;
	}
	
}
