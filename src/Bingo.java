/* Renato Cola�o e Bruno Maia */

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Bingo {

	// constantes, regras do jogo
	private static final int TAMANHO_CARTELA = 20;
	private static final int NUMERO_MINIMO = 1;
	private static final int NUMERO_MAXIMO = 99;
	
	// vari�veis de inst�ncia
	private List<Integer> numerosSorteados;
	private Globo globo;
	private List<Cartela> cartelas;
	private List<Cartela> cartelasVencedoras; // mais de uma pode bater ao mesmo tempo.
	private int quantidadeDeCartelas;
	private boolean temVencedor;
	
	// construtor
	public Bingo(int quantidadeDeCartelas){
		
		resete(quantidadeDeCartelas);
	}
	
	// m�todos privados
	private void inicializarCartelas(){
		
		cartelas = new ArrayList<Cartela>();
		
		for (int i = 0; i < quantidadeDeCartelas; i++) {
			
			cartelas.add(new Cartela(TAMANHO_CARTELA, NUMERO_MINIMO, NUMERO_MAXIMO));			
		}
	}
	
	private void adicionarNumeroSorteado(int numero) {

		//boolean existe = numerosSorteados.stream().anyMatch(n -> n == numero);
		numerosSorteados.add(numero);
	}
	
	private void marcarCartelas(int numero){
		
		for (Cartela cartela : cartelas) {
			cartela.marcarNumero(numero);			
		}
	}
	
	private void verificaVencedor(){
		
		for (Cartela cartela : cartelas) {
			if(cartela.todosNumerosMarcados()){
				cartelasVencedoras.add(cartela);
			}
		}
		
		if(cartelasVencedoras.size() > 0){
			temVencedor = true;
		}
	}
	
	private int sortearNumero(){
		
		// ao sortear um n�mero devemos retirar um n�mero do globo
		// adicionar este n�mero � lista de n�mero j� sorteados
		// marcar todas as cartelas
		// verificar se j� temos algum vencedor(a)
		
		int numeroSorteado = globo.sortearNumero();
		adicionarNumeroSorteado(numeroSorteado);
		marcarCartelas(numeroSorteado);
		verificaVencedor();
		
		return numeroSorteado;
	}
	
	private String exibirNumerosSorteados() {
		//List<Integer> list = numerosSorteados;
		//list = Collections.unmodifiableList(list);		
		return Arrays.toString(numerosSorteados.toArray());				
	}
	
	private int getQuantidadeDeCartelas(){
		return this.quantidadeDeCartelas;
	}

	private int getQuantidadeDeNumerosSorteados(){
		return numerosSorteados.size();
	}
	
	// m�todos p�blicos
	
	public void start(){
		
		// inicia simula��o de bingo
		
		System.out.println("Iniciando sorteios dos n�meros.\n");
		
		while (!temVencedor) {
			
			sortearNumero();
			//System.out.println("Sorteando n�mero: ");
			//System.out.println(sortearNumero());
			
		}
		
		System.out.println(String.format("Total de cartelas vendidas: %d.", getQuantidadeDeCartelas()));
		
		System.out.println("\nBingo! A cartela vencedora �:\n");
		
		for (Cartela cartela : cartelasVencedoras) {
						
			System.out.println(String.format("Identificador da cartela vencedora: %d.", cartela.getIdentificador()));
			System.out.println(cartela);
		}		
		
		System.out.println(String.format("\nForam chamados %d n�meros.", getQuantidadeDeNumerosSorteados()));
		
		System.out.println("\nOs n�meros sorteados foram: ");
		System.out.println(exibirNumerosSorteados());
	}
	
	public void resete(int quantidadeDeCartelas){
		
		// inicializa / reseta bingo
		
		this.quantidadeDeCartelas = quantidadeDeCartelas;
		this.temVencedor = false;
		numerosSorteados = new ArrayList<Integer>();
		globo = new Globo(NUMERO_MINIMO, NUMERO_MAXIMO);
		inicializarCartelas();
		cartelasVencedoras = new ArrayList<Cartela>();
	}	
}
