/* Renato Cola�o e Bruno Maia */

import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;

public final class BingoHelper {

	public static Integer[] getNumerosCartela(int tamanhoCartela, int numeroMinimo, int numeroMaximo){
		
		// escolhendo os 20 n�meros da cartela
		// entre os intervalos informados ex: 1 - 99
		
		// utilizando um set para evitar n�meros repetidos
		HashSet<Integer> numeros = new HashSet<>();
		
		while (numeros.size() < tamanhoCartela) {
			
			// nextInt is normally exclusive of the top value,
			// so add 1 to make it inclusive
			int random = ThreadLocalRandom
					.current().nextInt(numeroMinimo, numeroMaximo + 1);
			
			numeros.add(random);			
		}
		
		Integer[] arrNumeros = new Integer[numeros.size()];
		
		return numeros.toArray(arrNumeros);
		
	}

	public static Integer[] getNumerosGlobo(int numeroMinimo, int numeroMaximo){
		
		Integer[] numeros = new Integer[numeroMaximo];
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = numeroMinimo;
			numeroMinimo += 1;
		}
		
		return numeros;
		
	}

}
