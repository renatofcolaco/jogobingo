/* Renato Cola�o e Bruno Maia */

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Cartela {
	
	// constantes
	private final int TAMANHO_CARTELA;
	
	// variaveis staticas
	private static int contador = 0;
	
	// variaveis de inst�ncia
	// key � um n�mero da cartela e o bolean informa se o n�mero foi marcado ou n�o.
	private Map<Integer, Boolean> numerosCartela;
	private int identificador;
	
	// construtores
	public Cartela(int tamanhoCartela, int numeroMinimo, int numeroMaximo){
		
		TAMANHO_CARTELA = tamanhoCartela;
		inicializar(numeroMinimo, numeroMaximo);
		
		contador += 1;
		this.identificador = contador;
	}
	
	// m�todos privados
	private void inicializar(int numeroMinimo, int numeroMaximo){
		
		// pegando n�meros aleat�rios da cartela
		Integer[] arrNumerosCartela =
				BingoHelper.getNumerosCartela(TAMANHO_CARTELA, numeroMinimo, numeroMaximo);
		
		// Preenchendo a cartela		
		numerosCartela = new HashMap<Integer, Boolean>(TAMANHO_CARTELA);
		
		for (int i = 0; i < arrNumerosCartela.length; i++) {			
			numerosCartela.put(arrNumerosCartela[i], false);
		}
	}
	
	// m�todos p�blicos
	public int getIdentificador(){
		return this.identificador;
	}
	
	public void marcarNumero(Integer numero){
		
		for (Map.Entry<Integer, Boolean> entry : numerosCartela.entrySet()) {
			if(entry.getKey() == numero){
				entry.setValue(true);
				return;
			}
		}		
	}
	
	public boolean todosNumerosMarcados(){
		
		// verifica se uma cartela "bateu" o bingo
		// caso onde todos os n�meros da cartela est�o marcados (true)
		
		return numerosCartela.values().stream().allMatch(b -> b == true);		
	}
	
	@Override
	public String toString(){
		
		return "[" + numerosCartela
		.keySet()
		.stream()
		.map(k -> k.toString())
		.collect(Collectors.joining(", ")) + "]";
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + identificador;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cartela other = (Cartela) obj;
		if (identificador != other.identificador)
			return false;
		return true;
	}
}
